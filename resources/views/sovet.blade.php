<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <title>Попечительский совет</title>
    <link rel="shortcut icon" href="/image/logo-ico.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand logo" href="/"><img src="image/logo.png" alt="Logo" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        О Фонде
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/about-fund">О Фонде</a>
                        <a class="dropdown-item" href="/sharmanov">Об основателе фонда</a>
                        <a class="dropdown-item" href="/sovet">Попечительский совет</a>
                        <a class="dropdown-item" href="/our-team">Наша команда</a>
                        <a class="dropdown-item" href="/vacancy">Вакансии</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-project">Проекты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-news">Пресс-центр</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/reports-second">Отчеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Социальные сети
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Instagram</span></a>
                        <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Facebook</span></a>
                        <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Telegram</span></a>
                        <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Youtube</span></a>
                        <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Сайт Sharmanov.org</span></a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link color-link" href="/contacts">Рус</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Қаз</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Eng</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<hr class="line-header mb-5" width="100%">


<div class="container-xl mt-5 mb-5">
    <p class="sovet-name">Попечительский совет</p>
</div>

<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%9E%D0%BB%D0%B6%D0%B0%D1%81%20%D0%A1%D1%83%D0%BB%D0%B5%D0%B9%D0%BC%D0%B5%D0%BD%D0%BE%D0%B2.jpg" class="pop-sovet" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Олжас Сулейменов</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Олжас Сулейменов – поэт, писатель-литературовед, писатель, общественный и политический деятель, дипломат.
Литературное творчество и политическая активность сделали его одним из наиболее влиятельных общественных деятелей Центральной Азии. Стихи и поэмы Олжаса Сулейменова переведены на английский, французский, немецкий, испанский, чешский, польский, словацкий, болгарский, венгерский, монгольский и турецкий языки.
В 1989 году стал инициатором и лидером народного движения "Невада - Семипалатинск", благодаря деятельности которого был установлен мораторий на испытания, а потом и закрыт Семипалатинский ядерный полигон.
</p>
        </div>

    </div>
    </p>
</div>
<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%90%D1%81%D0%B0%D0%BD%D0%B0%D0%BB%D0%B8%20%D0%90%D1%88%D0%B8%D0%BC%D0%BE%D0%B2.jpg" class="pop-sovet-4" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Асанали Ашимов</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Асанали Ашимов – один из самых именитых казахстанских актёров и режиссёров театра и кино, заслуженный деятель искусств, народный артист СССР, народный артист Республики Казахстан, режиссер, педагог и сценарист.
В качестве режиссера-постановщика театра А. Ашимовым поставлены спектакли «Ревизор» Н. Гоголя, «Амангельды» Г. Мусрепова, «Есть ли яд, который я не пил…» И. Оразбаева, «Если б я был Султан» Саадама Ваннуса и др. В Шымкентском областном театре им. Ж. Шанина им поставлен спектакль «И даже Султан не вечен», в Республиканском театре юного зрителя им. Г. Мусрепова спектакль «Солнечная красавица Күнекей.
Снимался в кино с 1957 года, за его спиной роли в 28 фильмах, 4 режиссерские работы и 4 постановки как сценариста.
Широкую известность А. Ашимову принесли роли, сыгранные им в фильмах «Конец атамана», «Қыз-Жібек», «Транссибирский экспресс», «Кто вы, господин Ка?».
Асанали Ашимов награжден орденами «Отан», «Кұрмет», «Парасат», «Золотой орел» Международной академии ЮНЕСКО и российским орденом им. М. Ломоносова. Он является лауреатом Независимой общенациональной премии «Тарлан», обладателем почетного звания «Қазақстанның Еңбек Ері».</p>
        </div>

    </div>
    </p>
</div>
<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%9C%D1%83%D1%80%D0%B0%D1%82%20%D0%90%D1%83%D0%B5%D0%B7%D0%BE%D0%B2.jpg" class="pop-sovet" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Мурат Ауезов</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Мурат Ауезов – видный общественный деятель, ученый-культуролог, член Союза писателей Казахстана, Заслуженный деятель Республики Казахстан, лауреат премии «Тарлан», премии «Алтын асык» ассоциации «Золотой век».
Автор книг «Времен связующая нить», «Иппокрена», «Уйти, чтобы вернуться». В течение ряда лет он был послом Республики Казахстан в Китае. За это время провел огромную работу по организации посольства.
Мурат Ауезов внес значительный вклад в развитие науки, художественной литературы, искусства кино и телевидения Казахстана.
Один из создателей и лидеров неформального молодежного движения «Жас тұлпар», вице-президент антиядерного движения «Невада-Семипалатинск», модератор Семинара творческой и гуманитарной интеллигенции Центральной Азии «Беседы на Шелковом пути».

</p>
        </div>

    </div>
    </p>
</div>
<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%9A%D1%83%D0%B0%D0%BD%D1%8B%D1%88%20%D0%A1%D1%83%D0%BB%D1%82%D0%B0%D0%BD%D0%BE%D0%B2.jpg" class="pop-sovet-2" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Куаныш Султанов</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Куаныш Султанов – видный государственный и общественный деятель, публицист, первый министр печати и информации РК, бывший вице-премьер РК, экс-посол Казахстана в Китае, депутат Мажилиса Парламента РК V, VI созывов, депутат Сената Парламента РК II–IV созывов, доктор политических наук и профессор.
Куаныш Султанов сыграл значительную роль в новейшей истории страны. Его работа в качестве Чрезвычайного и Полномочного Посла Республики Казахстан в Китайской Народной Республике, КНДР и Вьетнаме по совместительству с 1995 по 2001 год стала весьма результативной для защиты внешнеполитических интересов Казахстана. За шесть лет деятельности на дип¬ломатическом поприще он внес большой вклад в развитие добрососедских отношений между странами, в успешное заключение договора о казахстанско-китайской границе.
При его активном и непосредственном участии принимались важные законы, касающиеся социальной защиты военнослужащих и обеспечения их жильем, совершенствования государственной службы и противодействия коррупции, мобилизации и мобилизационной подготовки, военного положения, других законов, направленных на обеспечение обороны и безопасности, улучшение социально-экономического положения и повышения уровня жизни населения страны.
</p>
        </div>

    </div>
</div>



<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%AE%D1%80%D0%B8%D0%B9%20%D0%9F%D1%8F.jpg" class="pop-sovet-6" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Юрий Пя</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Юрий Пя – известный во всем мире кардиохирург. Он внес значительный вклад в развитие здравоохранения Казахстана.
Под его руководством был основан Национальный научный кардиохирургический центр, где проводятся уникальные операции. С момента открытия центр стал клиникой международного масштаба, и является одним из топ 22 лучших кардиохирургических клиник мира.
В 2012 году Юрий Пя впервые в регионе провел операцию по трансплантации донорского сердца, а в 2016 году впервые в республике провел трансплантацию легких, которая является одной из сложнейших операций в трансплантологии. В 2017 году под его руководством впервые на международном уровне было имплантировано полностью искусственное сердце.
 Он является обладателем государственной награды "Қазақстанның Енбек Ерi", кавалером ордена "Құрмет" и ордена "Парасат", лауреат международной премии "Алтын Адам" в номинации "Врач года – 2012".
</p>
        </div>

    </div>
</div>
<div class="container-xl mt-5 mb-5">

    <div class="row">
        <div class="col-lg-6">
            <img src="image/pop-sovet/%D0%A1%D0%B5%D1%80%D0%B8%D0%BA%20%D0%90%D0%BA%D1%88%D1%83%D0%BB%D0%B0%D0%BA%D0%BE%D0%B2.jpg" class="pop-sovet-7" alt="">
        </div>
        <div class="col-lg-6">
            <h4 class="pb-4 sovet-name">Серик Акшулаков</h4>
            <p><span class="sovet-header">ЧЛЕН ПОПЕЧИТЕЛЬСКОГО СОВЕТА</span></p>
            <p class="sovet-text">Серик Акшулаков – известный во всем мире нейрохирург. Его вклад в развитие отечественной медицины неоценим, а научные достижения признаны во всем мире.
Серик Акшулаков выполняет наиболее сложные нейрохирургические операции при опухолях базальных отделов головного мозга, ствола мозга, желудочковой системы мозга с применением операционного микроскопа и микронейрохирургии. Им проведено более 5000 операций на головном мозге и позвоночнике. Он внедрил основы микронейрохирургии, эндоскопическую нейрохирургию (трансназальные эндоскопические подходы, эндоскопические операции при водянке головного мозга), впервые применил нейронавигацию в Казахстане, впервые в Казахстане осуществил оперативное лечение эпилепсии.
Под руководством и непосредственном участии Серика Акшулакова внедрены более 60 новых технологий, которые ранее не выполнялись в Казахстане, подготовлено и защищено 25 кандидатских и 5 докторских диссертаций, подготовлено более 30 клинических ординаторов и резидентов. Опубликовано более 300 научных работ (из них 80 в зарубежных журналах), в том числе 13 монографий, получено 13 патентов.
Лауреат Государственной премии РК в области науки и техники, «Қазақстанның Еңбек Ері». Председатель Правления АО «Национальный центр нейрохирургии», Президент Казахской ассоциации нейрохирургов.
</p>
        </div>

    </div>
</div>




<hr class="line" width="100%">

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-4 align-top">
                <img src="image/logo.png" alt="Logo" width="240px" class="footer-logo">
            </div>

            <div class="col-md-3 col-md-push-4 mt-3 social-icon-footer">
                            <p class="color-blue footer-social">Мы в социальных сетях</p>
                <div class="d-flex justify-content-around justify-content-end" style="width: 300px;">
                    <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a></div>
            </div>
        </div>
    </div>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body></html><?php
