<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <title>Наша Команда</title>
    <link rel="shortcut icon" href="/image/logo-ico.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand logo" href="/"><img src="image/logo.png" alt="Logo" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        О Фонде
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/about-fund">О Фонде</a>
                        <a class="dropdown-item" href="/sharmanov">Об основателе фонда</a>
                        <a class="dropdown-item" href="/sovet">Попечительский совет</a>
                        <a class="dropdown-item" href="/our-team">Наша команда</a>
                        <a class="dropdown-item" href="/vacancy">Вакансии</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-project">Проекты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-news">Пресс-центр</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/reports-second">Отчеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Социальные сети
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Instagram</span></a>
                        <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Facebook</span></a>
                        <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Telegram</span></a>
                        <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Youtube</span></a>
                        <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Сайт Sharmanov.org</span></a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link color-link" href="#">Рус</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Қаз</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Eng</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<hr class="line-header mb-5" width="100%">


<div class="container-xl">
    <div class="our-team">
        <h2 class="our-team-header">Наша команда</h2>
        <div class="row">
            <div class="col mb-3">
                <img src="image/team/our-team2.jpg" alt="" class="our-team-img">
            </div>
            <div class="col">
                <div class="our-team-info">
                    <h2 class="our-team-name">Сұлтанбек Бағдат Күмісбекұлы</h2>
                    <p class="our-team-post">ДИРЕКТОР ФОНДА</p>
                </div>
            </div>
        </div>
        <hr class="line-our-team" width="100%">
        <div class="row mb-3">
            <div class="col">
                <img src="/image/team/our-team3.JPG" alt="" class="our-team-img">
            </div>
            <div class="col">
                <div class="our-team-info">
                    <h2 class="our-team-name">Онланбаева Калжан Тулебаевна</h2>
                    <p class="our-team-post">БУХГАЛТЕР</p>
                </div>
            </div>
        </div>
    </div>
</div>





<hr class="line" width="100%">

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-4 align-top">
            <img src="image/logo.png" alt="Logo" width="240px" class="footer-logo">
        </div>

        <div class="col-md-3 col-md-push-4 mt-3 social-icon-footer">
            <p class="color-blue footer-social">Мы в социальных сетях</p>
            <div class="d-flex justify-content-around justify-content-end" style="width: 300px;">
                <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a></div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>
<?php
