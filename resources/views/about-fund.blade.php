<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <title>О Фонде</title>
    <link rel="shortcut icon" href="/image/logo-ico.ico" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand logo" href="/"><img src="image/logo.png" alt="Logo" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        О Фонде
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/about-fund">О Фонде</a>
                        <a class="dropdown-item" href="/sharmanov">Об основателе фонда</a>
                        <a class="dropdown-item" href="/sovet">Попечительский совет</a>
                        <a class="dropdown-item" href="/our-team">Наша команда</a>
                        <a class="dropdown-item" href="/vacancy">Вакансии</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-project">Проекты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-news">Пресс-центр</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/reports-second">Отчеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Социальные сети
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Instagram</span></a>
                        <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Facebook</span></a>
                        <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Telegram</span></a>
                        <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Youtube</span></a>
                        <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Сайт Sharmanov.org</span></a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link color-link" href="#">Рус</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Қаз</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Eng</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<hr class="line-header" width="100%">

<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <img src="image/about-fund/image%2032.png" alt="" class="about-fund-img-1">
        </div>
        <div class="col-lg-4 aboud-fund-col-img">
            <img src="image/about-fund/image%2033.png" alt="" class="about-fund-img-2">
            <img src="image/about-fund/image%2034.png" alt="" class="about-fund-img-2">
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-4">
            <h2 class="about-fund-col-title">Фонд</h2>
            <p class="about-fund-col-text">Международный фонд Торегельды Шарманова основан в начале 2021 года.
                Фонд является общественной неправительственной организацией, не
                преследующей политические и коммерческие цели.</p>
        </div>
        <div class="col-md-4">
            <h2 class="about-fund-col-title">Миссия фонда</h2>
            <p class="about-fund-col-text">Реализация всеобщего права и персональной ответственности за здоровье на основе улучшения здравоохранения, просвещения граждан об управлении здоровьем, развития науки и образования в области питания. </p>
        </div>
        <div class="col-md-4">
            <h2 class="about-fund-col-title">Основатель</h2>
            <p class="about-fund-col-text">Основателем и учредителем фонда является Торегельды Шарманов. Академик Торегельды Шарманов всю свою жизнь посвятил служению обществу, человечеству, улучшению здоровья и питания. Его огромный труд во имя процветания здравоохранения страны является образцом для многих. Жизненное кредо академика – помогать людям, быть полезным другим. </p>
        </div>
    </div>
</div>
<div class="container">
    <h2 class="about-fund-title">Основные направления деятельности фонда</h2>

    <div class="row">
        <div class="col-lg-2">
            <p class="about-fund-number">1</p>
        </div>
        <div class="col-lg-8">
            <h2 class="about-fund-destinations-margin">СОДЕЙСТВИЕ РЕАЛИЗАЦИИ ВСЕОБЩЕГО ПРАВА НА ЗДОРОВЬЕ</h2>
            <p class="about-fund-destinations-text">Продвижение идеи равного доступа к охране здоровья на основе первичной медико-санитарной помощи и социальной медицины путем присуждения ежегодных международных премий – казахстанскому специалисту-практику (коллективу) и зарубежному ученому (коллективу) </p>
            </div>
    </div>
    </div>
    <div class="container">
        <p class="about-fund-destinations-text">Просвещение населения по вопросам здорового образа жизни и предупреждения болезней на основе научно-обоснованного популярного контента о здоровье путем присуждения премий за публикации медийных продуктов о здоровье: статей, видеоматериалов, анимационных роликов, комиксов и пр.</p>
        <p class="about-fund-destinations-text">Развитие экосистемного здравоохранения путем содействия гражданам в принятии информированных решений касательно своего здоровья с помощью технологий и доступной информационной среды –– гранты и призовые конкурсы на создание мобильных приложений и других технологических продуктов в области медицины</p>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <h2 class="about-fund-destinations-margin-right text-left">Пропаганда здорового питания.</h2>
            <p class="about-fund-destinations-text-right text-left"><p class="about-fund-destinations-text">Просвещение населения по вопросам здорового образа жизни и предупреждения болезней на основе научно-обоснованного популярного контента о здоровье путем присуждения премий за публикации медийных продуктов о здоровье: статей, видеоматериалов, анимационных роликов, комиксов и пр.</p>
            <p class="about-fund-destinations-text">Развитие экосистемного здравоохранения путем содействия гражданам в принятии информированных решений касательно своего здоровья с помощью технологий и доступной информационной среды –– гранты и призовые конкурсы на создание мобильных приложений и других технологических продуктов в области медицины</p>
            </p>
            <p class="about-fund-destinations-text-right text-left">Адаптация казахских национальных блюд под современные принципы здорового питания – призовые конкурсы, реклама и другие формы продвижения новых рецептов здоровых национальных блюд</p>
        </div>
        <div class="col-lg-3">
            <p class="about-fund-number">2</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <p class="about-fund-number">3</p>
        </div>
        <div class="col-lg-9 about-fund-mb">
            <h2 class="about-fund-destinations-margin">РАЗВИТИЕ НАУКИ О ПИТАНИИ И ПРОФЕССИОНАЛЬНОГО ОБРАЗОВАНИЯ С ФОКУСОМ НА ВОПРОСЫ НУТРИЦИОЛОГИИ И ДИЕТОЛОГИИ</h2>
            <p class="about-fund-destinations-text">Поддержка научных и технологических проектов в области глобального питания и нутрициологии</p>
            <p class="about-fund-destinations-text">Предоставление грантов на стажировку специалистов в ведущих академических центрах, занимающихся вопросами нутрициологии и диетологии</p>
        </div>
    </div>
    </div>

<hr class="line" width="100%">

<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-push-4 align-top">
            <img src="image/logo.png" alt="Logo" width="240px" class="footer-logo">
        </div>

        <div class="col-md-3 col-md-push-4 mt-3 social-icon-footer">
            <p class="color-blue footer-social">Мы в социальных сетях</p>
            <div class="d-flex justify-content-around justify-content-end" style="width: 300px;">
                <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a>
                <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a></div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>
<?php
