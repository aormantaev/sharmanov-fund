
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <title>Төрегелдi Шарманов</title>
    <link rel="shortcut icon" href="/image/logo-ico.ico" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand logo" href="/"><img src="image/logo.png" alt="Logo" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        О Фонде
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/about-fund">О Фонде</a>
                        <a class="dropdown-item" href="/sharmanov">Об основателе фонда</a>
                        <a class="dropdown-item" href="/sovet">Попечительский совет</a>
                        <a class="dropdown-item" href="/our-team">Наша команда</a>
                        <a class="dropdown-item" href="/vacancy">Вакансии</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-project">Проекты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-news">Пресс-центр</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/reports-second">Отчеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Социальные сети
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Instagram</span></a>
                        <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Facebook</span></a>
                        <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Telegram</span></a>
                        <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Youtube</span></a>
                        <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Сайт Sharmanov.org</span></a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link color-link" href="#">Рус</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Қаз</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="#">Eng</a>
                </li>
            </ul>
        </div>
    </nav>
</div>


    <hr class="line-header" width="100%">

    <div class="container-main">
        <img src="image/sharmanov/sharmanov.jpg" alt="" class="image">
        <h2 class="main-image-header">Служение <br>добру и <br> здоровью</h2>
        <a class="btn btn-primary image-btn" href="/announce-project" role="button">Наши Проекты</a>
    </div>


    <div class="container">
        <img src="image/quote.jpeg" alt="" class="main-quote">
    </div>

    <div class="container">
        <h2 class="main-news-block-header">Новости</h2>

        <div class="row">
    <div class="col-lg-8">
        <img src="image/news/news.png" alt="" class="image-news">
        <div class="text-news">
        <h2 class="header-news pb-2">Наши Новости</h2>
        <p class="announce-news">Ознакомтесь с нашими новостями. <br>
            Анонсы самых важных и интересных новостей фонда.</p>
        </div>
    </div>
    <div class="col-lg-4">
        <h2 class="media-about-us-head">Сми о нас</h2>

        <div class="media-about-us">
            <p class="media-about-us-time">09.03.2021 в 12:02</p>
            <a href="https://tengrinews.kz/news/sozdan-mejdunarodnyiy-fond-toregeldyi-sharmanova-431021/" class="media-about-us-header" target="_blank">Создан международный фонд Торегельды Шарманова</a>
        </div>
<!--           <div class="media-about-us">-->
<!--            <p class="media-about-us-time">01.02.2021</p>-->
<!--               <a href="https://www.zdrav.kz/novosti/anons-obyavlyaetsya-konkurs-na-prisuzhdenie-premii-akademika-toregeldy-sharmanova" class="media-about-us-header" target="_blank">Премия академика Торегельды Шарманова</a>-->
<!--        </div>-->
<!--        <div class="media-about-us">-->
<!--            <p class="media-about-us-time">01.02.2021</p>-->
<!--            <a href="https://www.zdrav.kz/novosti/anons-obyavlyaetsya-konkurs-na-prisuzhdenie-premii-akademika-toregeldy-sharmanova" class="media-about-us-header" target="_blank">Премия академика Торегельды Шарманова</a>-->
<!--        </div>-->
    </div>
  </div>
    </div>

    <div class="container-xl mt-5">
         <div class="row">
    @foreach($posts as $post)
    <div class="col-lg-4">
      <div class="main-another-news-block">
          <img src="{{ Voyager::image( $post->image ) }}" alt="" class="main-another-news-image">
          <div class="main-another-news mt-3">
            <p class="main-another-news-time">{{\Carbon\Carbon::parse($post->created_at)->format('d.m.Y') }}</p>
              <h4 class="main-another-news-header"><a href="news/{{$post->slug}}" class="link-news">{{$post->title}}</a></h4>
            <p class="main-another-news-announce mt-2">{{$post->excerpt}}</p>
        </div>
      </div>
    </div>
   @endforeach
  </div>
    </div>


    <div class="container-xl mt-5 mb-3">
        <div class="row">
    <div class="col-md-6 col-md-push-6">
      <div class="main-project-block">
          <h2 class="main-project-block-header">Наши Проекты</h2>
          <p class="main-project-block-text">Ознакомтесь с нашими проектами.
              Анонсы самых важных и интересных программ и проектов.</p>
          <a class="btn btn-primary main-project-block-btn" href="/announce-project" role="button">Наши Проекты</a>
      </div>
    </div>
    <div class="col-md-6 col-md-push-6">
      <img src="image/annon-project-main.jpg" alt="" class="main-project-block-img">
    </div>
  </div>
    </div>



<!--<div class="container-fluid pb-5 mt-5 mg-t">-->
<!--        <div class="text-center">-->
<!--            <h1 class="title pt-5" id="partners">Наши Партнеры</h1>-->
<!---->
<!--            <div class="row">-->
<!--                <div class="col-lg partners"><img src="image/partner-4.png" alt="" style="max-height: 200px; max-width: 200px;"></div>-->
<!--                <div class="col-lg partners"><img src="image/clinic-2.png" alt="" style="height: 220px; width: 270px;"></div>-->
<!--                <div class="col-lg partners"><img src="image/clinic-3.png" alt="" style="height: 200px; width: 220px;"></div>-->
<!--                <div class="col-lg partners"><img src="image/mpk.png" alt="" style="height: 200px; width: 200px;"></div>-->
<!--                <div class="col-lg partners"><img src="image/invitro.jpg" alt="" style="height: 200px; width: 220px;"></div>-->
<!--                <div class="col-lg partners"><img src="image/med-solution.png" alt="" style="height: 180px; width: 220px;"></div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->




<hr class="line" width="100%">

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-4 align-top">
                <img src="image/logo.png" alt="Logo" width="240px" class="footer-logo">
            </div>

            <div class="col-md-3 col-md-push-4 mt-3 social-icon-footer">
                            <p class="color-blue footer-social">Мы в социальных сетях</p>
                <div class="d-flex justify-content-around justify-content-end" style="width: 300px;">
                    <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a></div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
    <script src="/js/libs.min.js"></script>
    <script src="/js/main.js"></script>
</body></html>
