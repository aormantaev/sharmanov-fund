<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Roboto:ital,wght@0,400;0,500;0,700;1,300&display=swap" rel="stylesheet">
    <title>Об основателе фонда</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand logo" href="/"><img src="image/logo.png" alt="Logo" class="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        О Фонде
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/about-fund">О Фонде</a>
                        <a class="dropdown-item" href="/sharmanov">Об основателе фонда</a>
                        <a class="dropdown-item" href="/sovet">Попечительский совет</a>
                        <a class="dropdown-item" href="/our-team">Наша команда</a>
                        <a class="dropdown-item" href="/vacancy">Вакансии</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-project">Проекты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/announce-news">Пресс-центр</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/reports-second">Отчеты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle color-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Социальные сети
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Instagram</span></a>
                        <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Facebook</span></a>
                        <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;">Twitter</span></a>
                        <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Youtube</span></a>
                        <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;">Сайт Sharmanov.org</span></a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link color-link" href="/contacts">Рус</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Қаз</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-link" href="/contacts">Eng</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<hr class="line-header" width="100%">

<div class="container-fluid">
    <img src="image/sharmanov/sharmanov.jpg" alt="" class="image">

</div>
<div class="container pt-3 pb-3">
    <p class="text-sharmanov-left"><span class="color-blue">Академик Торегельды Шарманов</span> всю свою жизнь посвятил служению обществу, человечеству, улучшению здоровья и питания. Его огромный труд во имя процветания здравоохранения страны является образцом для многих. Жизненное кредо академика – помогать людям, быть полезным другим.</p>
    <p class="text-sharmanov-right"><span class="color-blue">Торегельды Шарманов</span> один из крупных организаторов медицины в стране, основатель школы нутрициологов в Казахстане и республиках Центральной Азии. Он внес огромный вклад в мировое и казахстанское здравоохранение, инициировав проведение в 1978 году в Алматы Международной конференции по первичной медико-санитарной помощи под эгидой ВОЗ и ЮНИСЕФ. По итогам конференции была принята Алма-Атинская Декларация – один из основополагающих документов в мировом здравоохранении, в котором впервые изложена концепция организации национальной системы первичной медико-санитарной помощи.</p>
    <p class="text-sharmanov-left">Он основатель и президент <span class="color-blue">Казахской Академии Питания</span>. Руководитель международных научных проектов по ликвидации железодефицитной анемии, дефицита витамина А и фолиевой кислоты, йододефицита, поддержке грудного вскармливания, питания космонавтов, спортсменов, разработке лечебно-профилактических продуктов питания для детей, беременных женщин, а также лиц, страдающих хроническими заболеваниями.  Среди прочего, им разработаны уникальные продукты на основе кобыльего молока. </p>
    <p class="text-sharmanov"><span class="color-blue">Академик Т. Шарманов</span> был удостоен высшей награды Всемирной Организации Здравоохранения – медали Леона Бернарда за выдающийся вклад в мировое здравоохранение.
    </p>
    <p class="text-sharmanov">Дополнительную  информацию о биографии Торегельды Шарманова можете узнать на данном сайте:
        <br>
        <br><button type="button" class="btn btn-outline-primary"><a href="https://sharmanov.org/ru" target="_black">Sharmanov.org</a></button></p>
</div>
<hr class="line" width="100%">

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-4 align-top">
                <img src="image/logo.png" alt="Logo" width="240px" class="footer-logo">
            </div>

            <div class="col-md-3 col-md-push-4 mt-3 social-icon-footer">
                            <p class="color-blue footer-social">Мы в социальных сетях</p>
                <div class="d-flex justify-content-around justify-content-end" style="width: 300px;">
                    <a class="dropdown-item" href="https://www.instagram.com/toregeldysharmanov/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px;"></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.facebook.com/toregeldysharmanuly/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://t.me/toregeldysharmanov" target="_blank"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 2em;color: #0897D3; margin-right: 15px; "></i><span style="padding-left: 2px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://www.youtube.com/channel/UCUauUTIx2v2gPBs_5hgmaxg/featured" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a>
                    <a class="dropdown-item" href="https://sharmanov.org/ru" target="_blank"><i class="fa fa-globe" aria-hidden="true" style="font-size: 2em;color: #0897D3"></i><span style="padding-left: 10px; font-size: 18px; color: #0897D3;"></span></a></div>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>
<?php
