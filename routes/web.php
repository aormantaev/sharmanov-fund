<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('about-fund', function () {
    return view('about-fund');
});
Route::get('sharmanov', function () {
    return view('sharmanov');
});
Route::get('sovet', function () {
    return view('sovet');
});
Route::get('our-team', function () {
    return view('our-team');
});
Route::get('vacancy', function () {
    return view('anchor');
});
Route::get('vacancy-second', function () {
    return view('vacancy-second');
});
Route::get('announce-project', 'HomeController@getprojects');
//Route::get('announce-project', function () {
//    return view('anchor');
//});
Route::get('projects/{slug}','HomeController@getoneproject');
//Route::get('projects/{id}', function () {
//    return view('anchor');
//});
Route::get('announce-news', 'HomeController@getanons');
Route::get('news/{id}','HomeController@showNews');

Route::get('reports', function () {
    return view('anchor');
});
Route::get('reports-second', function () {
    return view('reports-second');
});
Route::get('contacts', function () {
    return view('contacts');
});



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
