<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    public function index(){

        $posts = Post::where('category_id', 1)->take(3)->orderby('created_at','DESC')->get();

        return view('index',compact('posts',$posts));
    }

    public function getanons(){

        $posts = Post::where('category_id', 1)->take(8)->orderby('created_at','DESC')->get();
        $posts2 = Post::where('category_id', 1)->take(8)->orderby('created_at','ASC')->get();

        return view('announce-news',['posts' => $posts, 'posts2' => $posts2]);

    }
    public function showNews($slug){

        $post = Post::where('slug',$slug)->first();

        return view('news',['post' => $post]);


    }
    public function getprojects(){

        $featured_project = Post::where('featured',1)->first();
        $projects = Post::where('category_id', 2)->orderby('created_at','ASC')->get();

        return view('announce-project',['projects' => $projects, 'featured_project' => $featured_project]);

    }

    public function getoneproject($slug){

        $featured_project = Post::where('slug',$slug)->first();

        return view('project-fund',[ 'featured_project' => $featured_project]);

    }

}
